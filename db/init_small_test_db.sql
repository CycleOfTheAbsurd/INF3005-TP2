DELETE FROM animal;
DELETE FROM user;

insert into user values (0, 'Gilles', 'Proulx', '1112223333', '123 address', 'abc@email.com', 'salt', 'hash', 0);
insert into user values (1, 'Gilles', 'Proulx', '1112223333', '123 address', 'abc@email.com', 'salt', 'hash', 1);
insert into user values (2, 'Gilles', 'Proulx', '1112223333', '123 address', 'abc@email.com', 'salt', 'hash', 2);
insert into user values (3, 'Gilles', 'Proulx', '1112223333', '123 address', 'abc@email.com', 'salt', 'hash', 3);
insert into user values (4, 'Gilles', 'Proulx', '1112223333', '123 address', 'abc@email.com', 'salt', 'hash', 4);
insert into user values (5, 'Gilles', 'Proulx', '1112223333', '123 address', 'abc@email.com', 'salt', 'hash', 5);
