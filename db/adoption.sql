create table animal (
  id integer primary key,
  name varchar(25),
  type varchar(25),
  race varchar(25),
  age integer,
  description varchar(50),
  photo blob,
  id_user integer,
  foreign key (id_user) references user(id)
);

create table user (
  id integer primary key,
  first_name varchar(25),
  last_name varchar(25),
  phone varchar(20),
  address varchar(50),
  email varchar(50),
  salt varchar(32),
  hash varchar(128),
  id_animal integer,
  foreign key (id_animal) references animal(id)
);

create table session (
  id integer primary key,
  id_session varchar(32),
  email varchar(50)
);

create table reset_token (
  id integer promary key,
  token varchar(36),
  expiration integer,
  id_user integer,
  foreign key (id_user) references user(id)
);
