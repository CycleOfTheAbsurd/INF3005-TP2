#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import sqlite3

animals_data = [
    (0, 'Sylvestre', 'cat', 'siamois', 60, 'pussycat',
     'db/testimages/Sylvestre.png', 0),
    (1, 'pitou', 'dog', 'berger allemand', 20, 'good boye',
     'db/testimages/Pitou.jpg', 1),
    (2, 'Josélito', 'iguana', 'verte', 60, 'grosse criss diguane verte',
     'db/testimages/Joselito.jpg', 2),
    (3, 'harambe', 'monkey', 'fat monkey', 60, 'RIP',
     'db/testimages/Harambe.jpg', 3),
    (4, 'petit poney', 'horse', 'small', 60, 'tout gris et tout petit',
     'db/testimages/Petit_Poney.jpg', 4),
    (5, 'Marie Laberge', 'skunk', 'original', 60, 'la pas fine',
     'db/testimages/Marie_Laberge.jpg', 5),
]

connection = sqlite3.connect('db/Adoption.db')

for animal in animals_data:
    cursor = connection.cursor()
    with open(animal[6], "rb") as image:
        cursor.execute("INSERT INTO animal VALUES (?, ?, ?, ?, ?, ?, ?, ?);",
                       (animal[0], animal[1], animal[2], animal[3], animal[4],
                        animal[5], sqlite3.Binary(image.read()), animal[7]))
    connection.commit()

connection.close()
