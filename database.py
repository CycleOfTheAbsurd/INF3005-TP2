#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import sqlite3
import uuid
import hashlib
import time
from datetime import timedelta

from .helpers import tuple_to_animal_dict
from .helpers import tuple_to_animal_owner_dict
from .helpers import tuple_to_owner_dict
from .helpers import hash_password
from .helpers import format_phone_db

ANIMALS_PER_PAGE = 5


class Database:
    def __init__(self):
        self.connection = None

    def get_connection(self):
        if self.connection is None:
            self.connection = sqlite3.connect('db/Adoption.db')
        return self.connection

    def disconnect(self):
        if self.connection is not None:
            self.connection.close()
            self.connection = None

    def get_animal_detail(self, animal_id):
        cursor = self.get_connection().cursor()
        cursor.execute("SELECT * FROM animal WHERE id=?",
                       (str(animal_id),))
        try:
            animal = cursor.fetchone()
        except TypeError:
            return None
        else:
            return tuple_to_animal_dict(animal)

    def get_animal_list(self, start):
        cursor = self.get_connection().cursor()
        count = cursor.execute("SELECT COUNT(*) FROM animal").fetchone()[0]
        cursor.execute("SELECT * FROM animal LIMIT ?,?", (
            start,
            ANIMALS_PER_PAGE
        ))

        try:
            animals = cursor.fetchall()
            animals = [tuple_to_animal_dict(animal) for animal in animals]
        except TypeError:
            return [], count
        else:
            return animals, count

    def get_animals_with_owner(self):
        cursor = self.get_connection().cursor()
        cursor.execute("SELECT animal.id, animal.name, animal.type, \
                       animal.race, animal.age, animal.description, \
                       user.id, user.first_name, user.last_name, \
                       user.phone, user.address, user.email \
                       FROM animal LEFT JOIN user ON animal.id_user = user.id")
        animal_owner = cursor.fetchall()
        try:
            animal_owner = [tuple_to_animal_owner_dict(animal)
                            for animal in animal_owner]
        except TypeError:
            return None
        return animal_owner

    def get_owner_contact_info(self, animal_id):
        cursor = self.get_connection().cursor()
        cursor.execute("""SELECT id, first_name, last_name, phone,
                        address, email FROM user WHERE id_animal = ?""",
                       (animal_id,))
        owner = cursor.fetchone()
        if owner is None:
            return None
        owner_info = tuple_to_owner_dict(owner)
        return owner_info

    def search_animal(self, search_term, start):
        cursor = self.get_connection().cursor()
        cursor.execute("SELECT * FROM animal\
                        WHERE type LIKE ? \
                        OR race LIKE ? \
                        OR description LIKE ? \
                        OR name LIKE ?", (
                        "%" + search_term + "%",
                        "%" + search_term + "%",
                        "%" + search_term + "%",
                        "%" + search_term + "%",
                       ))
        try:
            animals = cursor.fetchall()
            count = len(animals)
            animals = [tuple_to_animal_dict(animal) for animal in animals]
        except TypeError:
            return [], count
        else:
            return animals[start: start + ANIMALS_PER_PAGE], count

    def get_animals(self):
        cursor = self.get_connection().cursor()
        cursor.execute("SELECT * FROM animal")
        animals = cursor.fetchall()

        if animals is None:
            return None
        else:
            return [tuple_to_animal_dict(animal) for animal in animals]

    def get_animal_image(self, animal_id):
        cursor = self.get_connection().cursor()
        cursor.execute("SELECT photo FROM animal WHERE id=?",
                       (animal_id,))
        animal = cursor.fetchone()
        if animal is None:
            return None
        else:
            return animal[0]

    def create_user(self, form):
        salt = uuid.uuid4().hex
        hashed_password = hash_password(salt, form.password.data)
        user_exists = self.get_user(form.email.data)
        formated_phone = format_phone_db(form.phone_number.data)
        if not user_exists:
            cursor = self.get_connection().cursor()
            cursor.execute(("""INSERT INTO user(first_name, last_name,
                           phone, address, email, salt, hash)
                           values(?, ?, ?, ?, ?, ?, ?)"""),
                           (form.first_name.data, form.last_name.data,
                           formated_phone, form.address.data, form.email.data,
                           salt, hashed_password))
            self.get_connection().commit()
            return True, None
        else:
            return False, """Cette adresse courriel existe déjà dans la base
                          de donnée... Veuillez réessayer avec une autre
                          adresse"""

    def create_animal(self, form, obj_user, photo):
        connection = self.get_connection()
        cursor = connection.cursor()
        if photo is None:
            photo_db = None
        else:
            photo_db = sqlite3.Binary(photo)

        cursor.execute("""INSERT INTO animal(name, type, race,
                       age, description, photo, id_user)
                       values(?, ?, ?, ?, ?, ?, ?)""",
                       (form.name.data, form.type.data, form.race.data,
                        form.age.data, form.description.data,
                        photo_db, obj_user["id"]))
        cursor.execute("select last_insert_rowid()")
        last_id = cursor.fetchone()[0]
        cursor.execute("UPDATE user SET id_animal=? WHERE id=?",
                       (last_id, obj_user["id"]))
        connection.commit()

    def update_user(self, form, email):
        connection = self.get_connection()
        cursor = connection.cursor()
        formated_phone = format_phone_db(form.phone_number.data)
        cursor.execute(("""UPDATE user SET first_name=?, last_name=?,
                       phone=?, address=? WHERE email=?"""),
                       (form.first_name.data, form.last_name.data,
                       formated_phone, form.address.data, email))
        connection.commit()

    def connect_user(self, form):
        cursor = self.get_connection().cursor()
        cursor.execute("SELECT salt, hash FROM user WHERE email=?",
                       (form.connect_email.data,))
        user = cursor.fetchone()
        if user is None:
            return False, """Cette adresse courriel n'est associée à aucun
                             compte dans notre base de donnée..."""
        else:
            salt = user[0]
            password = form.connect_password.data
            hashed_password = hashlib.sha512(str(password + salt)
                                             .encode("utf-8")).hexdigest()
            if hashed_password == user[1]:
                return True, None
            else:
                return False, """L'adresse courriel et le mot de passe entrés
                              ne correspondent pas aux informations dans notre
                              base de données."""

    def get_user(self, email):
        cursor = self.get_connection().cursor()
        cursor.execute("""SELECT id, first_name, last_name, phone, address,
                        email FROM user WHERE email=?""", (email,))
        user = cursor.fetchone()

        if user is not None:
            return tuple_to_owner_dict(user)
        else:
            return None

    def insert_token(self, token, user):
        expiration_time = int(timedelta(minutes=30)
                              .total_seconds()) + int(time.time())
        connection = self.get_connection()
        cursor = connection.cursor()
        cursor.execute("""INSERT INTO reset_token(token, expiration,
                       id_user) values(?, ?, ?)""",
                       (token, expiration_time, user["id"]))
        connection.commit()

    def reset_password(self, form, token):
        connection = self.get_connection()
        cursor = connection.cursor()
        cursor.execute("""SELECT * FROM reset_token WHERE token=?""",
                       (token,))
        db_token = cursor.fetchone()
        if db_token is None or int(time.time()) > db_token[2]:
            return False
        else:
            salt = uuid.uuid4().hex
            password = form.password.data
            hashed_password = hashlib.sha512(str(password + salt)
                                             .encode("utf-8")).hexdigest()
            cursor.execute(("""UPDATE user SET salt=?, hash=?
                           WHERE id=?"""),
                           (salt, hashed_password, db_token[3]))
            connection.commit()
            return True

    def check_user_has_animal(self, email):
        cursor = self.get_connection().cursor()
        cursor.execute("""SELECT id, id_animal FROM user
                       WHERE email=?""", (email,))
        user = cursor.fetchone()
        return user[1] is not None

    # Methode inspirée de cet exemple :
    # https://github.com/jacquesberger/exemplesINF3005/blob/master/Flask/login/database.py
    def save_session(self, id_session, email):
        connection = self.get_connection()
        connection.execute(("INSERT INTO session(id_session, email) "
                            "values(?, ?)"), (id_session, email))
        connection.commit()

    # Methode inspirée de cet exemple :
    # https://github.com/jacquesberger/exemplesINF3005/blob/master/Flask/login/database.py
    def delete_session(self, id_session):
        connection = self.get_connection()
        connection.execute(("DELETE FROM session WHERE id_session=?"),
                           (id_session,))
        connection.commit()

    # Methode inspirée de cet exemple :
    # https://github.com/jacquesberger/exemplesINF3005/blob/master/Flask/login/database.py
    def get_session(self, id_session):
        cursor = self.get_connection().cursor()
        cursor.execute(("SELECT email FROM session WHERE id_session=?"),
                       (id_session,))
        data = cursor.fetchone()
        if data is None:
            return None
        else:
            return data[0]
