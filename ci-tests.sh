#!/bin/bash

valid=0
for file in *.py
do
	python3 -m pycodestyle "$file"
	if [ $? != 0 ]
	then
		valid=1
	fi
done
exit $valid
