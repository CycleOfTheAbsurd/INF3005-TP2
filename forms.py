#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from wtforms import Form
from wtforms import StringField
from wtforms import TextAreaField
from wtforms import PasswordField
from wtforms import IntegerField
from wtforms import FileField
from wtforms.fields.html5 import EmailField
from wtforms import validators

ERROR_REQUIRED = "Ce champ doit être rempli."
ERROR_INTEGER = "Ce champ doit être un nombre."
ERROR_LONG = "Ce champ est trop long."
ERROR_AGE = "Nous refusons de spéculer sur la naissance d'éventuels animaux"
ERROR_EMAIL = "Ce champ doit contenir une adresse courriel valide."
ERROR_TOO_SHORT = "Le message doit contenir au moins 10 caractères."
ERROR_IMAGE = "Le fichier doit être une image valide."
ERROR_PHONE_FORMAT = """Le format du numéro de téléphone n'est pas le bon.
                        Il doit être: 999 999 9999"""

PHONE_REGEX = '^(\\+?\\d[\\s-])?\\d{3}[\\s-]\\d{3}[\\s-]\\d{4}$'

IMAGE_REGEX = r'^[^\\/\\\]\.(?:png|jpg|jpeg|bmp|gif|ico)$'


class ContactOwnerForm(Form):
    name = StringField("Votre Nom", [
        validators.required(message=ERROR_REQUIRED)])
    source_email = EmailField("Votre Adresse Courriel", [
        validators.required(message=ERROR_REQUIRED),
        validators.email(message=ERROR_EMAIL)])
    phone_number = StringField("Votre Numéro de Téléphone (Optionnel)")
    message = TextAreaField("Message", [
        validators.required(message=ERROR_REQUIRED),
        validators.length(min=10, message=ERROR_TOO_SHORT)])


class CreateUserForm(Form):
    first_name = StringField("Prénom", [
        validators.required(message=ERROR_REQUIRED),
        validators.Length(max=25, message=ERROR_LONG)])
    last_name = StringField("Nom", [
        validators.required(message=ERROR_REQUIRED),
        validators.Length(max=25, message=ERROR_LONG)])
    phone_number = StringField("Numéro de téléphone", [
        validators.required(message=ERROR_REQUIRED),
        validators.Regexp(PHONE_REGEX, message=ERROR_PHONE_FORMAT)
    ])
    address = TextAreaField("Adresse", [
        validators.required(message=ERROR_REQUIRED),
        validators.Length(max=25, message=ERROR_LONG)])
    email = EmailField("Courriel", [
        validators.required(message=ERROR_REQUIRED),
        validators.email(message=ERROR_EMAIL),
        validators.Length(max=50, message=ERROR_LONG)])
    password = PasswordField('Mot de passe', [
        validators.required(message=ERROR_REQUIRED)
    ])


class AdoptionForm(Form):
    name = StringField("Nom", [
        validators.required(message=ERROR_REQUIRED),
        validators.Length(max=25, message=ERROR_LONG)])
    type = StringField("Type", [
        validators.required(message=ERROR_REQUIRED),
        validators.Length(max=25, message=ERROR_LONG)])
    race = StringField("Race", [
        validators.required(message=ERROR_REQUIRED),
        validators.Length(max=25, message=ERROR_LONG)])
    age = IntegerField("Age", [
        validators.required(message=ERROR_INTEGER),
        validators.NumberRange(min=0, message=ERROR_AGE)])
    description = TextAreaField("Description", [
        validators.required(message=ERROR_REQUIRED),
        validators.Length(max=50, message=ERROR_LONG)])


class ConnectUserForm(Form):
    connect_email = StringField("Courriel", [
        validators.required(message=ERROR_REQUIRED)
    ])
    connect_password = PasswordField('Mot de passe', [
        validators.required(message=ERROR_REQUIRED)
    ])


class ResetPasswordForm(Form):
    password = PasswordField("Mot de passe", [
        validators.required(message=ERROR_REQUIRED)
    ])


class ForgotPasswordForm(Form):
    email = StringField("Courriel", [
        validators.required(message=ERROR_REQUIRED),
        validators.email(message=ERROR_EMAIL)
    ])


class UpdateUserForm(Form):
    first_name = StringField("Prénom", [
        validators.required(message=ERROR_REQUIRED),
        validators.Length(max=25, message=ERROR_LONG)
    ])
    last_name = StringField("Nom", [
        validators.required(message=ERROR_REQUIRED),
        validators.Length(max=25, message=ERROR_LONG)
    ])
    phone_number = StringField("Numéro de téléphone", [
        validators.required(message=ERROR_REQUIRED),
        validators.Regexp(PHONE_REGEX, message=ERROR_PHONE_FORMAT)
    ])
    address = TextAreaField("Adresse", [
        validators.required(message=ERROR_REQUIRED),
        validators.Length(max=25, message=ERROR_LONG)])
    email = EmailField("Courriel")
