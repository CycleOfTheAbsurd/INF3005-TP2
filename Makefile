PHONY: install run check database

install:
	pip3 install --user -r requirements.txt

database:
	if [ -f "db/Adoption.db" ]; then rm -f db/Adoption.db; fi;

	echo ".quit" | sqlite3 -batch -init db/adoption.sql db/Adoption.db
	echo ".quit" | sqlite3 -batch -init db/init_small_test_db.sql db/Adoption.db
	python3 db/init_small_test_values.py

run: install
	FLASK_APP=index.py flask run

check:
	./ci-tests.sh
