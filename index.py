#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from datetime import datetime
import time

from flask import Flask
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import jsonify
from flask import session
from flask import make_response
from flask import Response
from functools import wraps

from .database import Database
from .helpers import *
from .forms import *

app = Flask(__name__, static_url_path="/static", static_folder="static")

ANIMALS_PER_PAGE = 5


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        g._database = Database()
    return g._database


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.disconnect()


@app.route('/')
def home_page():
    signin_form = ConnectUserForm(request.form)
    email = None
    if "id" in session:
        email = get_db().get_session(session["id"])
    animals = get_db().get_animals()
    if (animals is None) or (len(animals) == 0):
        five_animals = None
    else:
        five_animals = get_five_random_animals(animals)
    return render_template('accueil.html',
                           signin_form=signin_form,
                           animals=five_animals,
                           email=email)


@app.route("/animal/<animal_id>")
def animal_page(animal_id):
    signin_form = ConnectUserForm(request.form)
    email = None
    if "id" in session:
        email = get_db().get_session(session["id"])
    animal = get_db().get_animal_detail(animal_id)
    form = ContactOwnerForm(request.form)
    if animal is not None:
        return render_template("animal_page.html",
                               signin_form=signin_form,
                               animal=animal,
                               form=form,
                               email=email)
    else:
        return render_template("404.html", signin_form=signin_form), 404


@app.route("/animals/", defaults={"page": 1})
@app.route('/animals/page/<int:page>')
def animal_list(page):
    signin_form = ConnectUserForm(request.form)
    email = None
    if "id" in session:
        email = get_db().get_session(session["id"])
    animals, count = get_db().get_animal_list((page - 1) * ANIMALS_PER_PAGE)
    if len(animals) == 0 and page != 1:
        return render_template("404.html", signin_form=signin_form), 404
    pagination = Pagination(page, ANIMALS_PER_PAGE, count)
    return render_template("animal_list.html",
                           signin_form=signin_form,
                           pagination=pagination,
                           animals=animals,
                           email=email)


@app.route("/animals/images/<animal_id>")
def animal_image(animal_id):
    image_binary = get_db().get_animal_image(animal_id)
    if image_binary is None:
        signin_form = ConnectUserForm(request.form)
        return render_template("404.html", signin_form=signin_form), 404

    response = make_response(image_binary)
    mimetype = get_mimetype(image_binary)
    if mimetype is None:
        return abort(400)
    response.headers.set('Content-Type', mimetype)
    return response


@app.route("/animals/search/", defaults={"page": 1}, methods=["GET"])
@app.route("/animals/search/page/<int:page>", methods=["GET"])
def animals_search_results(page):
    signin_form = ConnectUserForm(request.form)
    email = None
    if "id" in session:
        email = get_db().get_session(session["id"])
    search_term = request.args.get('search')
    if search_term is None or search_term == "":
        return redirect("/animals/")
    animals, count = get_db().search_animal(search_term,
                                            (page - 1) * ANIMALS_PER_PAGE)
    if len(animals) == 0 and page != 1:
        return render_template("404.html", signin_form=signin_form), 404
    pagination = Pagination(page, ANIMALS_PER_PAGE, count)
    return render_template("animal_list.html",
                           signin_form=signin_form,
                           pagination=pagination,
                           animals=animals,
                           search=search_term,
                           email=email)


@app.route("/api/animals/", methods=["GET"])
def animal_api_json():
    animals = get_db().get_animals_with_owner()
    return jsonify(animals)


@app.route("/inscription", methods=["GET"])
def signup_page():
    form = CreateUserForm(request.form)
    signin_form = ConnectUserForm(request.form)
    return render_template("signup.html",
                           signin_form=signin_form,
                           form=form)


@app.route("/inscription/submit", methods=["POST"])
def signup_page_submit():
    form = CreateUserForm(request.form)
    signin_form = ConnectUserForm(request.form)

    if request.method == "POST" and form.validate():
        user_created, error = get_db().create_user(form)

        if user_created:
            return redirect("/inscription/confirmation")
        else:
            return render_template("signup.html",
                                   signin_form=signin_form,
                                   form=form,
                                   error=error)
    return render_template("signup.html",
                           form=form,
                           signin_form=signin_form)


@app.route("/inscription/confirmation")
def signup_page_confirm():
    signin_form = ConnectUserForm(request.form)
    return render_template("signup_confirm.html",
                           signin_form=signin_form)


@app.route("/connexion", methods=["POST"])
def connection_user():
    signin_form = ConnectUserForm(request.form)
    if signin_form.validate():
        user, error = get_db().connect_user(signin_form)

        if user is False:
            return render_template("erreur_connexion.html",
                                   error_signin=error,
                                   signin_form=signin_form)
        else:
            id_session = uuid.uuid4().hex
            get_db().save_session(id_session, signin_form.connect_email.data)
            session["id"] = id_session
            return redirect("/profil")
    return redirect("connexion/erreur")


@app.route("/connexion/erreur")
def connection_user_error():
    signin_form = ConnectUserForm(request.form)
    user, error = get_db().connect_user(signin_form)
    return render_template("erreur_connexion.html",
                           error_signin=error,
                           signin_form=signin_form)


@app.route("/contact/", methods=["POST"])
def contact_owner():
    form = ContactOwnerForm(request.form)
    if form.validate():
        owner_info = get_db().get_owner_contact_info(request.form["animal_id"])
        send_contact_email(request.form, owner_info)
        return jsonify({"message": "Votre message a été envoyé"})
    return jsonify(data=form.errors), 400


@app.route("/reset_password/")
def forgot_password():
    form = ForgotPasswordForm()
    signin_form = ConnectUserForm(request.form)
    return render_template("forgot_password.html",
                           signin_form=signin_form,
                           form=form)


@app.route("/reset_password/submit", methods=["POST"])
def forgot_password_submit():
    form = ForgotPasswordForm(request.form)
    signin_form = ConnectUserForm(request.form)
    if request.method == "POST" and form.validate():
        user_obj = get_db().get_user(form.email.data)
        if user_obj is not None:
            token = uuid.uuid4().hex
            get_db().insert_token(token, user_obj)
            send_password_token_email(user_obj, token)

        else:
            error = """Cette adresse courriel n'est associée à aucun
                             compte dans notre base de donnée..."""
            return render_template("forgot_password.html",
                                   signin_form=signin_form,
                                   form=form,
                                   error=error)

    return render_template("forgot_password.html",
                           signin_form=signin_form,
                           form=form)


@app.route("/reset_password/<token>", methods=["GET", "POST"])
def reset_password(token):
    form = ResetPasswordForm(request.form)
    signin_form = ConnectUserForm(request.form)
    if request.method == "POST" and form.validate():
        password_changed = get_db().reset_password(form, token)
        if password_changed:
            return render_template("profil_confirm.html",
                                   signin_form=signin_form)
        else:
            error = """Le token est invalide ou expiré,
                       <a href="/reset_password">Vous pouvez
                       en demander un autre ici</a>"""
            return render_template("reset_password.html",
                                   form=form,
                                   signin_form=signin_form,
                                   error=error)
    else:
        return render_template("reset_password.html",
                               form=form,
                               signin_form=signin_form)


# Methode prise de cet exemple :
# https://github.com/jacquesberger/exemplesINF3005/blob/master/Flask/login/index.py
def authentication_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if not is_authenticated(session):
            return send_unauthorized()
        return f(*args, **kwargs)
    return decorated


@app.route("/adoption/", endpoint='adoption', methods=["GET", "POST"])
@authentication_required
def adoption():
    form = AdoptionForm(request.form)
    email = get_db().get_session(session["id"])
    obj_user = get_db().get_user(email)
    if get_db().check_user_has_animal(email):
        return render_template("erreur_adoption.html",
                               email=email,
                               form=form)
    else:
        if request.method == "POST" and form.validate():
            photo = None
            if len(request.files) != 0:
                photo = request.files['photo_name'].read()
                photo_mimetype = get_mimetype(photo)
                if photo_mimetype is None:
                    photo = None  # Si la photo n'est pas une image, on lignore
            get_db().create_animal(form, obj_user, photo)
            return redirect("/adoption/confirmation")
        else:
            return render_template("adoption.html",
                                   email=email,
                                   form=form)


@app.route("/adoption/confirmation", endpoint='adoption_confirm',
           methods=["GET"])
@authentication_required
def adoption_confirm():
    email = get_db().get_session(session["id"])

    return render_template("adoption_confirm.html",
                           email=email)


@app.route("/profil", endpoint='user_page', methods=["GET"])
@authentication_required
def user_page():
    email = get_db().get_session(session["id"])
    obj_user = get_db().get_user(email)
    form = UpdateUserForm()
    form.first_name.data = obj_user["first_name"]
    form.last_name.data = obj_user["last_name"]
    form.phone_number.data = obj_user["phone_number"]
    form.address.data = obj_user["address"]
    form.email.data = obj_user["email"]

    return render_template("user_page.html",
                           email=email,
                           form=form)


@app.route("/profil/submit", methods=["GET", "POST"])
@authentication_required
def user_page_submit():
    email = get_db().get_session(session["id"])
    form = UpdateUserForm(request.form)

    if request.method == "POST" and form.validate():
        get_db().update_user(form, email)
        return redirect("/profil/confirmation")
    else:
        return render_template("user_page.html",
                               email=email,
                               form=form)


@app.route("/profil/confirmation")
@authentication_required
def user_page_confirm():
    email = get_db().get_session(session["id"])

    return render_template("profil_confirm.html",
                           email=email)


# Methode prise de cet exemple :
# https://github.com/jacquesberger/exemplesINF3005/blob/master/Flask/login/index.py
@app.route('/logout', endpoint='logout')
@authentication_required
def logout():
    id_session = session["id"]
    session.pop('id', None)
    get_db().delete_session(id_session)
    return redirect("/")


# Methode prise de cet exemple :
# https://github.com/jacquesberger/exemplesINF3005/blob/master/Flask/login/index.py
def is_authenticated(session):
    return "id" in session


# Methode prise de cet exemple :
# https://github.com/jacquesberger/exemplesINF3005/blob/master/Flask/login/index.py
@app.route('/logout')
def send_unauthorized():
    return Response('Could not verify your access level for that URL.\n'
                    'You have to login with proper credentials', 401,
                    {'WWW-Authenticate': 'Basic realm="Login Required"'})


with open("config.yml") as configfile:
        config = yaml.load(configfile)

app.secret_key = config["secret_key"]
