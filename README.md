# TP2 INF3005

Ce projet est réalisé dans le cadre du cours INF3005 - Programmation Web Avancée à la session d'hiver 2018.

Il s'agit d'un d'un site d'adoption d'animaux. Ce site permet à des utilisateurs de créer un compte pour mettre un animal en adoption. Les animaux disponibles sont visibles sur le site et les visiteurs peuvent contacter les propriétaires de ces animaux avec un formulaire qui envoie un courriel.

### Disclaimer

Nous nous trouvions très drôle avec le branding de notre site __JBerger Allemand__ pardonnez-nous si la blague est de mauvais goût.

# Dépendances

 - [Flask](http://flask.pocoo.org/)
 - [WTForms](http://wtforms.readthedocs.io/en/latest/)

# Installation

Pour installer les dépendances du projet, il suffit de lancer la cible 'install' du [Makefile](Makefile) avec la commande suivante:

`make install`

Il est aussi possible de le faire directement avec pip avec la commande suivante:

`pip3 install --user -r requirements.txt`

# Configuration

La base de données peut être initialisée avec des valeurs de test en utilisant la cible 'database' du [Makefile](Makefile).

`make database`

Pour faire fonctionner l'envoi de courrier, vous devez créer une copie du fichier [config.sample.yml](config.sample.yml), la nommer `config.yml` et remplacer les placeholders par les vraies informations de connection de votre compte gmail.

# Exécution

Lancer la cible 'run' du [Makefile](Makefile) avec la commande:

`make run`

Ceci lancera le serveur de développement de Flask sur le port 5000.

# Auteurs

 - Alexandre Côté Cyr - COTA07049302
 - Louis-Bernard Poulin - POUL26099201

# License

Ce project est sous licence [GPL3](LICENSE.md).

# Documentation de l'API

L'api n'a qu'une seule route, `/api/animals/` qui retourne une liste de tous les animaux disponibles.

Comme cette route ne fait que donner de l'information, la seule méthode acceptée est GET.
