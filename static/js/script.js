// fonction inspirée de :
// https://github.com/jacquesberger/exemplesINF2005/blob/master/Javascript/validation.html
function clearErrorMessagesUpdate() {
  document.getElementById("error_firstname").innerHTML = "";
  document.getElementById("error_lastname").innerHTML = "";
  document.getElementById("error_address").innerHTML = "";
  document.getElementById("error_phone").innerHTML = "";
}

// fonction inspirée de :
// https://github.com/jacquesberger/exemplesINF2005/blob/master/Javascript/validation.html
function clearErrorMessagesAdoption() {
  document.getElementById("error_name").innerHTML = "";
  document.getElementById("error_type").innerHTML = "";
  document.getElementById("error_race").innerHTML = "";
  document.getElementById("error_age").innerHTML = "";
  document.getElementById("error_description").innerHTML = "";
}

// fonction inspirée de :
// https://github.com/jacquesberger/exemplesINF2005/blob/master/Javascript/validation.html
function clearErrorMessagesSignup() {
  document.getElementById("error_firstname").innerHTML = "";
  document.getElementById("error_lastname").innerHTML = "";
  document.getElementById("error_phone").innerHTML = "";
  document.getElementById("error_address").innerHTML = "";
  document.getElementById("error_email").innerHTML = "";
  document.getElementById("error_pwd").innerHTML = "";
}

// fonction inspirée de :
// https://github.com/jacquesberger/exemplesINF2005/blob/master/Javascript/validation.html
function clearErrorMessagesForgotPassword() {
  document.getElementById("error_email").innerHTML = "";
}

// fonction inspirée de :
// https://github.com/jacquesberger/exemplesINF2005/blob/master/Javascript/validation.html
function validateRequiredField(inputId, spanId) {
  var value = document.getElementById(inputId).value;
  if (value == null || value === "") {
    document.getElementById(spanId).innerHTML = "Ce champ doit contenir une valeur!";
    return false;
  }
  return true;
}

// fonction inspirée de :
// https://github.com/jacquesberger/exemplesINF2005/blob/master/Javascript/validation.html
function validatePhoneFormat(inputId, spanId) {
  var value = document.getElementById(inputId).value;
  var patt = /^(\+?\d[\s-])?\d{3}[\s-]\d{3}[\s-]\d{4}$/
  if (!patt.test(value)) {
    document.getElementById(spanId).innerHTML = "Le format du numéro de téléphone n'est pas le bon.";
    return false;
  }
  return true;
}

// fonction inspirée de :
// https://github.com/jacquesberger/exemplesINF2005/blob/master/Javascript/validation.html
function validateEmailFormat(inputId, spanId) {
  var value = document.getElementById(inputId).value;
  var patt = /^.+\@.+\..+$/
  if (!patt.test(value)) {
    document.getElementById(spanId).innerHTML = "Le format de l'adresse courriel n'est pas le bon.";
    return false;
  }
  return true;
}

// fonction inspirée de :
// https://github.com/jacquesberger/exemplesINF2005/blob/master/Javascript/validation.html
function checkRequiredFieldsSignup() {
  clearErrorMessagesSignup();
  var field1 = validateRequiredField("first_name", "error_firstname");
  var field2 = validateRequiredField("last_name", "error_lastname");
  var field3 = validateRequiredField("phone_number", "error_phone");
  var field4 = validateRequiredField("address", "error_address");
  var field5 = validateRequiredField("email", "error_email");
  var field6 = validateRequiredField("password", "error_pwd");
  var field7 = validatePhoneFormat("phone_number", "error_phone");
  var field8 = validateEmailFormat("email", "error_email");
  return field1 && field2 && field3 && field4 && field5 && field6 && field7 && field8;
}

// fonction inspirée de :
// https://github.com/jacquesberger/exemplesINF2005/blob/master/Javascript/validation.html
function checkRequiredFieldsAdoption() {
  clearErrorMessagesAdoption();
  var field1 = validateRequiredField("name", "error_name");
  var field2 = validateRequiredField("type", "error_type");
  var field3 = validateRequiredField("race", "error_race");
  var field4 = validateRequiredField("age", "error_age");
  var field5 = validateRequiredField("description", "error_description");
  return field1 && field2 && field3 && field4 && field5;
}

// fonction inspirée de :
// https://github.com/jacquesberger/exemplesINF2005/blob/master/Javascript/validation.html
function checkRequiredFieldsUpdate() {
  clearErrorMessagesUpdate();
  var field1 = validateRequiredField("first_name", "error_firstname");
  var field2 = validateRequiredField("last_name", "error_lastname");
  var field3 = validateRequiredField("phone_number", "error_phone");
  var field4 = validateRequiredField("address", "error_address");
  var field5 = validatePhoneFormat("phone_number", "error_phone");
  return field1 && field2 && field3 && field4 && field5;
}

// fonction inspirée de :
// https://github.com/jacquesberger/exemplesINF2005/blob/master/Javascript/validation.html
function checkRequiredFieldsForgotPassword() {
  clearErrorMessagesForgotPassword();
  var field1 = validateRequiredField("email", "error_email");
  var field2 = validateEmailFormat("email", "error_email");
  return field1 && field2;
}

function clearErrorMessages() {
    $("[id$=_error]").remove();
}

function sendContactForm(f) {
    clearErrorMessages();
    $.ajax({
        type: "POST",
        url: "/contact/",
        dataType: "json",
        data: $("#contactForm").serialize(),
        success: function(data) {
            $("#contactForm").replaceWith("<div class='alert alert-success'>" + data["message"] + "</div>");
        },
        error: function(request, status_message, error) {
            console.log(request);
            if(request.status == 400) {
                for(var field in request.responseJSON["data"]) {
                        $("#" + field + "_div").append("<p class='alert alert-danger' id='"
                            + field + "_error'>" + request.responseJSON["data"][field] + "</p>");
                }
            }
        }

    });
    f.preventDefault();
    return false;
}

$(document).ready(function() {
  $("#adoptionForm").submit(checkRequiredFieldsAdoption);
  $("#signupForm").submit(checkRequiredFieldsSignup);
  $("#passwordForm").submit(checkRequiredFieldsForgotPassword);
  $("#updateForm").submit(checkRequiredFieldsUpdate);
  $("#contactForm").submit(sendContactForm);
});
