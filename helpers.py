import smtplib
import yaml
import random
import hashlib
import uuid
import magic

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from math import ceil
from flask import render_template

VALID_MIMETYPES = ["image/bmp", "image/gif", "image/jpg",
                   "image/jpeg", "image/png", "image/x-icon"]


def send_email(from_address, to_address, content):
    with open("config.yml") as configfile:
        config = yaml.load(configfile)
    source_address = config["email"]
    destination_address = to_address
    body = content
    subject = "Message de JBerger Allemand"

    msg = MIMEMultipart()
    msg['Subject'] = subject
    msg['From'] = source_address
    msg['To'] = destination_address
    msg['ReplyTo'] = from_address

    msg.attach(MIMEText(body, 'plain'))

    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.ehlo()
    server.starttls()
    server.login(source_address, config["password"])
    text = msg.as_string()
    server.sendmail(source_address, destination_address, text)
    server.quit()


def send_contact_email(contact_form, owner_info):
    from_address = contact_form["source_email"]
    to_address = owner_info["email"]
    content = render_template("contact_email.txt",
                              sender=contact_form, owner=owner_info)
    send_email(from_address, to_address, content)


def send_password_token_email(user_info, token):
    with open("config.yml") as configfile:
        config = yaml.load(configfile)
    from_address = config["email"]
    to_address = user_info["email"]
    content = render_template("password_reset_email.txt",
                              user=user_info,
                              token=token)
    send_email(from_address, to_address, content)


def tuple_to_animal_dict(animal_tuple):
    keys = ["id", "name", "type", "race", "age",
            "description", "url_photo", "id_user"]
    return dict(zip(keys, animal_tuple))


def tuple_to_animal_owner_dict(animal_owner_tuple):
    animal_keys = ["id", "name", "type", "race", "age", "description"]
    owner_keys = ["id", "first_name", "last_name",
                  "phone_number", "address", "email"]
    animal_dict = dict(zip(animal_keys, animal_owner_tuple[:6]))
    owner_dict = dict(zip(owner_keys, animal_owner_tuple[6:]))
    animal_dict["owner"] = owner_dict
    return animal_dict


def tuple_to_owner_dict(owner_tuple):
    owner_keys = ["id", "first_name", "last_name",
                  "phone_number", "address", "email"]
    return dict(zip(owner_keys, owner_tuple))


def get_five_random_animals(list_animals):
    if list_animals is None:
        return None
    else:
        return random.sample(list_animals, min(len(list_animals), 5))


def hash_password(salt, password):
    hashed_password = hashlib.sha512(str(password +
                                         salt).encode("utf-8")).hexdigest()
    return hashed_password


def format_phone_db(phone_number):
    phone_number.replace(" ", "")
    return phone_number


def get_mimetype(binary):
    m = magic.Magic(mime=True)
    mimetype = m.from_buffer(binary)
    if mimetype is None or mimetype not in VALID_MIMETYPES:
        return None
    return mimetype


class Pagination():
    def __init__(self, page, per_page, total_count):
        self.page = page
        self.per_page = per_page
        self.total_count = total_count

    @property
    def pages(self):
        return int(ceil(self.total_count / float(self.per_page)))

    @property
    def prev(self):
        if self.page > 1:
            return self.page - 1
        else:
            return None

    @property
    def next(self):
        if self.page < self.pages:
            return self.page + 1
        else:
            return None
